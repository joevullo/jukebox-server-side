# README for Jukebox Server 
This project contains the server side code for jukebox project. The code is written using the Django Rest Framework. 


### Initial setup of project 
django-admin.py startproject projectname
python manage.py startapp appname

Add the following to projectname/settings.py:
INSTALLED_APPS = (
    ...
    'rest_framework',
    'appname',
) 

Add something like the following to projectname/urls.py: 

urlpatterns = [
    url(r'^', include('snippets.urls')),
]

### Setup the requirements  
pip install -r requirements.txt
or:
pip install django
pip install djangorestframework
pip install djangorestframework-gis
pip install django-filter

The project now requires the installation of Geo-Django (Django library for Gelocation) on the system:
 1. sudo apt-get install binutils libproj-dev gdal-bin 
    Install the geolocation libraries run on debian/ubuntu
    (https://docs.djangoproject.com/en/1.8/ref/contrib/gis/install/geolibs/)
 2. sudo apt-get install mysql-server mysql-client
    Install MySQL because it spatial types already. 
    Also install the python connector: pip install mysql-python
    If any issues occur when trying to install mysql-python then debian may be missing the following packages:
       sudo apt-get install libmysqlclient-dev
       sudo apt-get install python-mysqldb
       (perhaps using easy_install MySQL-python if needed)


For SQL you also need to setup a database for the system to use:

    mysql -u root -h localhost -p 
    CREATE DATABASE jukeboxdb CHARACTER SET utf8;
    [Create a new user for the database]
    CREATE USER 'jukeboxuser'@'localhost' IDENTIFIED BY 'ashgabsjg12512'; #Do not use this password.
    GRANT ALL PRIVILEGES ON *jukeboxdb . * TO 'jukeboxuser'@'localhost';


Make sure the config for the database (in settings.py) looks something like this:

DATABASES = {
    'default': {
       # 'ENGINE': 'django.db.backends.mysql', 
        'ENGINE':'django.contrib.gis.db.backends.mysql',
        'NAME': 'jukeboxdb',
        'USER': 'jukeboxuser',
        'PASSWORD': 'ashgabsjg12512',
        'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    }
}

 3. Alternatively Spatialite is also a good solution. (Especially after running into some SQL problems)
    Install the relevant libraries:
      sudo apt-get install libsqlite3-dev    
      sudo apt-get install libspatialite3 spatialite-bin 
      sudo apt-get install libspatialite3 
      sudo apt-get install sqlite3  

    Test with this, correct if nothing runs after:
    sqlite> CREATE VIRTUAL TABLE testrtree USING rtree(id,minX,maxX,minY,maxY);

    $ wget https://pypi.python.org/packages/source/p/pysqlite/pysqlite-2.6.3.tar.gz
    $ tar xzf pysqlite-2.6.3.tar.gz
    $ cd pysqlite-2.6.3
    
    Next, use a text editor to edit the setup.cfg file to look like the following:

    [build_ext]
    #define=
    include_dirs=/usr/local/include
    library_dirs=/usr/local/lib
    libraries=sqlite3
    #define=SQLITE_OMIT_LOAD_EXTENSION

    $ python setup.py install
    
    # Settings.py looks like this:

    DATABASES = {
    'default': {
       # 'ENGINE': 'django.db.backends.mysql', 
       # 'ENGINE':'django.contrib.gis.db.backends.mysql',
        'ENGINE':'django.contrib.gis.db.backends.spatialite',
        'NAME': 'jukeboxdb',
        'USER': 'root',
        'PASSWORD': 'mehsat1992',
        'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
      }
    }


The project now installs the following packages to implement an easier solution 
for user authentication and registration. 

pip install django-rest-auth
pip install django-allauth

A full and easy setup guide is available at: http://django-rest-auth.readthedocs.org/en/latest/installation.html

### Sets up the environment.
virtualenv env
source env/bin/activate 

Virtual env allows us to have an environment that stores 
packages needed for pip.  

### Migrates the database.
python manage.py makemigrations
python manage.py migrate

Make migrations will ensure any changes needed for the database to the schema. 
Migrate will make changes aswell. 

### Used to create a new user.
python manage.py createsuperuser

Creates a user in Django with special priviledges. 

### Used to reveal current SQL database schema. 
python manage.py sql research

Used to find any issues by researching what schema has been stored within the database this time. 

### Clean the database. 
python manage.py flush
python manage.py syncdb

Sometimes this will not be enough for some issues, and you will have to find the sql file and remove it.
The db file is stored in the base level directory for the project.

Be careful, syncdb is deprecated and superseded by "migrate" and "makemigrations"

### Auto gen some users
There is a script in the top directory (the one with manage.py in it) called  
startup.py. Once run this will add 5 auto generated users to the database.
It needs to be run using the python shell so that the correct libraries are included.
 like so:

python manage.py shell
>>> execfile('startup.py')
>>> exit()


## Curl command to create a new user. 
$ curl -X POST -d username=new -d email=new@example.com -d is_staff=false -H 'Accept: application/json; indent=4' -u admin:password http://127.0.0.1:8000/users/



