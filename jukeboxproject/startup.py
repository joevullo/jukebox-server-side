from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from jukebox.models import UserStatus, Friend, Messages, MusicLibrary, Jukebox, Song, Location
from jukebox.permissions import IsOwnerOrReadOnly
from jukebox.serializers import UserStatusSerializer, UserSerializer, FriendSerializer, MessagesSerializer
from jukebox.serializers import MusicLibrarySerializer, JukeboxSerializer, SongSerializer, LocationSerializer
from rest_framework import mixins, generics, models
from rest_framework_gis.filters import DistanceToPointFilter
import random

# Created to add some users in the database when wanted. 

print ("Creating 5 random users.")
for x in range(0, 5):
    random_val = random.randint(0,99999) #Should be a 5 digit number. 
    gen_username  = ("user", random_val)
    #gen_useremail = 'user%@email.com' % (random_val)
    gen_useremail = ('user' + str(random_val) + "@gmail.com")

    user = User.objects.create_user(
    username = gen_username,
    password = 'password',
    email = gen_useremail, 
    )
    user.save()
    print("Gen user = ", gen_username)

print("Finished.")
