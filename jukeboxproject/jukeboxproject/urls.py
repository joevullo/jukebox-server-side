from jukebox import views
from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'status', views.UserStatusViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'friends', views.FriendViewSet)
#router.register(r'currentplaylist', views.ViewSet)
#router.register(r'jukebox', views.JukeboxViewSet)
router.register(r'messages', views.MessagesViewSet)
router.register(r'music_library', views.MusicLibraryViewSet)

# For testing
router.register(r'songs', views.SongViewSet)

# This is for the geo library views. TODO
geo_urls = patterns('',
    #url(r'^/(?P<pk>\d+)/photos$', PostPhotoList.as_view(), name='postphoto-list'),
    #url(r'^/(?P<pk>\d+)$', PostDetail.as_view(), name='post-detail'),
    #url(r'^$', PostList.as_view(), name='post-list')
)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^accounts/', include('allauth.urls'))
)


