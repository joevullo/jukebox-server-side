from rest_framework import serializers
from jukebox.models import UserStatus, Friend, Messages, MusicLibrary, Song, Jukebox, Location
from django.contrib.auth.models import User
from rest_framework_gis.serializers import GeoFeatureModelSerializer

class LocationSerializer(GeoFeatureModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """

    class Meta:
        model = Location
        geo_field = "point"

        # you can also explicitly declare which fields you want to include
        # as with a ModelSerializer.
        fields = ('id', 'address', 'city', 'state')

class UserStatusSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(source='pk', read_only=True)
    username = serializers.CharField(source='user.username', read_only=True) #This doesn't seem to work. 
    
    class Meta:
        model = UserStatus
        fields = ('url','id','owner','username','test_info','created_at', 
        'updated_at', 'geolocation', 'jukebox_mode_enabled',)

	read_only_fields = ('created_at', 'updated_at', 
        'owner', 'username', 'url', 'id',) 

        write_only_fields = ('owner')

# TODO 
#    # Create & update are used to place data in to the database. 
#    def update(self, instance, validated_data):
#        instance.test_info = validated_data.get('test_info', instance.title)
#        return instance

#    def create(self, validated_data):
#        return JukeboxUser.objects.create(user=user, **validated_data

class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('id','url', 'username','first_name', 'last_name', 
        'email','user_status', 'password', 'user_friends','user_messages','user_library')

        write_only_fields = ('password', 'owner')
        read_only_fields = ('user_status','id', 'user_friends','user_messages','user_library')
    
    # Using these to ensure that the password is hased still if it is changed.
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance

class SongSerializer(serializers.HyperlinkedModelSerializer):
    #id = serializers.IntegerField(source='pk', read_only=True)

    class Meta: 
        model = Song
        fields = ('id','title','artist','album','albumId','num_votes','cleared')
        read_only_fields = ('song_id')

class JukeboxSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(source='pk', read_only=True)

    class Meta: 
        model = Jukebox
        fields = ('id',)

class MusicLibrarySerializer(serializers.HyperlinkedModelSerializer):
    #id = serializers.IntegerField(source='pk', read_only=True)
    #songs = serializers.HyperlinkedRelatedField(queryset=Song.objects.all(), view_name='song-detail', many=True)
    songs = SongSerializer(many=True)  # A nested list of 'song' items


    class Meta: 
        model = MusicLibrary
	#fields = ('id','owner','url','songs','otherSongs')
        fields = ('owner','url','songs')
        #fields = ('owner','url',)

class MessagesSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(source='pk', read_only=True)
    suggested_songs = SongSerializer(many=True, required=False)

    class Meta: 
        model = Messages
        fields = ('id','owner','url','suggested_songs')
        #fields = ('id','url','suggested_songs',)

#    def create(self, validated_data):
#        user = validated_data.get('user')
#        # Get our categories
#        sug_songs_data = validated_data.pop('suggested_songs')
#        # Create our item
#        message = Messages(**validated_data) 
#        #message = Messages.objects.create(**validated_data)
#        # Process the categories. We create any new categories, or return the ID of existing
#        # categories.
#        for song in sug_songs_data:
#            #song['name'] = song['name'].title()
#            song, created = Song.objects.get_or_create(**song)
#            message.suggested_songs.add(song.id)
#        message.save()
#        return message

#    def create(self, validate_data):
#        subitemdata = validate_data.pop('suggested_songs')
#        subitemobj = Song.objects.create(**subitemdata)
#        return Messages.objects.create(sub_items=subitemobj, **validate_data)

#    def create(self, validated_data):
#        song_data = validated_data.pop('suggested_songs')
#        messages = Messages.objects.create(**validated_data)
#        for song in song_data:
#            s = Song.objects.create()
#            s.song = song.get("song")
#            s.save()
#            messages.suggested_songs.add(s)
#        post.save()
#        return message

    # Closest I've been so far. 
    def create(self, validated_data):
        suggested_songs_data = validated_data.pop('suggested_songs')
        suggest_song_list = list()
        for song_data in suggested_songs_data:
            song = Song.objects.create(**song_data)
            suggest_song_list.append(song)          
        message = Messages.objects.create(suggested_songs=suggest_song_list, **validated_data)
        return message

    # This doesn't work either. 
#    def create(self, validated_data):
#        song_data = validated_data.pop('suggested_songs')
#        message = Messages.objects.create(**validated_data)
#        for song in song_data:
#            try:
#                song = Song.objects.get(title=song["title"])
#            except Song.DoesNotExist:
#                song = Song.objects.create(**song)
#            message.suggested_songs.add(song) # 'NoneType' object has no attribute 'add' 
#        return message

#    def create(self, validated_data):
#        suggested_songs_data = validated_data.pop('suggested_songs')
#        for song_data in suggested_songs_data:
#
#            profile = Profile.objects.create(user=user,**profile_data)
#
#        Message = Message.objects.create(suggested_songs=songs, **validated_data)
#        return user


#    def create(self, validated_data):
#        song_data = validated_data.pop('suggested_songs')
#        song = Song.objects.create(**song_data)
        # song need to be created first because the foreign key is in
        # the Messages model
#        message = Messages.objects.create(suggested_songs=song, **validated_data)
#        return message

#   This one is the closest and it kind of works. 
#    def update(self, instance, validated_data):
#        songs = validated_data.pop('suggested_songs')
#        updated_instance = super(MessagesSerializer, self).update(instance, validated_data)
#        updated_instance.save()
#        for song_data in songs:
#            s = Song(**song_data)
#            s.save()
#            updated_instance.suggested_songs.add(s)

        #updated_instance.save()
#        return updated_instance



    # This works 
    def update(self, instance, validated_data):
        suggested_songs_data = validated_data.pop('suggested_songs')
        updated_instance = super(MessagesSerializer, self).update(instance, validated_data) 
        updated_instance.save()
        for song_data in suggested_songs_data:	
            #songID = song_data.pop('id') # Grabbing the song ID so that we can check if the ID already exists. 
            #print(songID)
	    s = Song(**song_data)
            print(song_data)
            print(instance)
            print("HELLO")
            print(suggested_songs_data)
        # If the song already exists we want to edit  
            print("+++ DEBUG ------------------------- DEBUG +++")
            s.id = '20'
            print(s.id)
            print(s.title)
	    if Song.objects.filter(id = s.id).exists():
                print("FOUND OBJECT")
                # If the object exists, then we should edit it. 
                foundSong = Song.objects.get(id=s.id)
                print(foundSong.title)
            # We should be editing song. 
                foundSong.title = s.title
                foundSong.artist = s.artist
                foundSong.album = s.album
                foundSong.albumId = s.albumId
                foundSong.num_votes = s.num_votes 
                foundSong.cleared = s.cleared
                foundSong.save()
            else:
                # no object satisfying query exists, so lets create a new one. 
                s.save()
                updated_instance.suggested_songs.add(s)
        return updated_instance


    # This works 
#    def update(self, instance, validated_data):
#        suggested_songs_data = validated_data.pop('suggested_songs')
#        updated_instance = super(MessagesSerializer, self).update(instance, validated_data) 
#        updated_instance.save()
#        for song_data in suggested_songs_data:	
        #songID = song_data.pop('id') # Grabbing the song ID so that we can check if the ID already exists. 
#	    s = Song(**song_data)
#            print(song_data)
#            print(s.id)
#            print("HELLO")
#            print(suggested_songs_data)
        # If the song already exists we want to edit  
#            print("+++ DEBUG ------------------------- DEBUG +++")
#            s.id = '20'
#            print(s.id)
#            print(s.title)
#	    if Song.objects.filter(id = s.id).exists():
#                print("FOUND OBJECT")
#                # If the object exists, then we should edit it. 
#                foundSong = Song.objects.get(id=s.id)
#                print(foundSong.title)
#            # We should be editing song. 
#                foundSong.title = s.title
#                foundSong.artist = s.artist
#                foundSong.album = s.album
#                foundSong.albumId = s.albumId
#                foundSong.num_votes = s.num_votes 
#                foundSong.cleared = s.cleared
#                foundSong.save()
#            else:
                # no object satisfying query exists, so lets create a new one. 
#                s.save()
#                updated_instance.suggested_songs.add(s)
#        return updated_instance

#     def update(self, instance, validated_data):
#         suggested_songs = validated_data.pop('suggested_songs')
#         profile = instance.suggested_songs

#         updated_instance = super(MessagesSerializer, self).update(instance, validated_data)

#         profile.foo = profile_data.get('foo', profile.foo)
#         profile.bar = profile_data.get('bar', profile.bar)
#         profile.save()

#         return instance

class FriendSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(source='pk', read_only=True)
 
    class Meta: 
        model = Friend
        fields = ('id','owner','url','friends',)
