from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from jukebox.models import UserStatus, Friend, Messages, MusicLibrary, Jukebox, Song, Location
from jukebox.permissions import IsOwnerOrReadOnly, IsSuperuser, IsFriendOrOwner
from jukebox.serializers import UserStatusSerializer, UserSerializer, FriendSerializer, MessagesSerializer
from jukebox.serializers import MusicLibrarySerializer, JukeboxSerializer, SongSerializer, LocationSerializer
from rest_framework import mixins, generics, models
from rest_framework_gis.filters import DistanceToPointFilter

####
#
# This file contains the views that will be shown to the user using the models  
# and serializers.  
# 
####

class LocationList(generics.ListAPIView):

    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    distance_filter_field = 'geometry'
    filter_backends = (DistanceToPointFilter, )
    bbox_filter_include_overlapping = True # Optional



# This is a view set that can be used to limit the available options 
# that a user can perform on the API. This will allow 
# a user to view and update existing objects, but not
# create new ones. 
class MainViewSet(mixins.UpdateModelMixin,
                                #mixins.ListModelMixin,
                                mixins.RetrieveModelMixin,
                                viewsets.GenericViewSet):
    pass

# This is a view set that can be used to limit the available options 
# that a user can perform on the API. This will allow 
# a user to view and update existing objects, but not
# create new ones. 
class UpdateListRetrieveViewSet(mixins.UpdateModelMixin,
                                mixins.ListModelMixin,
                                mixins.RetrieveModelMixin,
                                viewsets.GenericViewSet	):
    pass

class JukeboxViewSet(UpdateListRetrieveViewSet):
    """
    The Jukebox can  
    
    Permissions:
    """

    queryset = Jukebox.objects.all()
    serializer_class = JukeboxSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly, IsSuperuser)


class MessagesViewSet(viewsets.ModelViewSet):
#class MessagesViewSet(UpdateListRetrieveViewSet):
    """
    Messages are used to passed suggested songs to users. Message objects 
    are autocreated when a new user is created and linked to that user using the 
    same ID. When a user is deleted the associated messages object will also be deleted. 
    
    Permissions:  
        * Users must be authenticated. 
        * Only friends of users should be available to view this information. 
        * New objects cannot be created, as they are tied to users. 
    
    """

    queryset = Messages.objects.all()
    serializer_class = MessagesSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,
    #permission_classes = (IsSuperuser,)
    #                     IsOwnerOrReadOnly, IsSuperuser)

class MusicLibraryViewSet(UpdateListRetrieveViewSet):

    queryset = MusicLibrary.objects.all()
    serializer_class = MusicLibrarySerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,
    #                      IsOwnerOrReadOnly,)

#class FriendViewSet(UpdateListRetrieveViewSet):
class FriendViewSet(viewsets.ModelViewSet):

    queryset = Friend.objects.all()
    serializer_class = FriendSerializer

class UserStatusViewSet(UpdateListRetrieveViewSet):
#class UserStatusViewSet(viewsets.ModelViewSet):

    #queryset = UserStatus.objects.all()
    queryset = UserStatus.objects.all().filter(owner=2)
    serializer_class = UserStatusSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly)
    #permission_classes = (IsSuperuser,)


    #def perform_create(self, serializer):
    #    serializer.save(owner=self.request.user)

class SongViewSet(viewsets.ModelViewSet):
    
    queryset = Song.objects.all()
    serializer_class = SongSerializer

class UserViewSet(viewsets.ModelViewSet):
    
    queryset = User.objects.all()
    serializer_class = UserSerializer

