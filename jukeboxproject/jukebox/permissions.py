from rest_framework import permissions

# Permission rules are: 
  # Users can view all their own information, and update that information, they have all levels of access.  
  # Friends can view other users information, but only edit some information such as... 
  # Unauthenticated users cannot view any information, other than registration. 
  # 
#
# The following are defined as "Safe" methods. 
# SAFE_METHODS = ['GET', 'HEAD', 'OPTIONS'] 

# This is the permission described in the tutorial. 
class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet
        return obj.owner == request.user

# This class limits some views for only the superuser in django. 
class IsSuperuser(permissions.BasePermission):
    """
    Custom permission used to limit some views to just the superuser.
    """

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True

        #return obj.owner == request.user

class IsFriendOrOwner(permissions.BasePermission):

    """
    This permission allows friends to view information about other 
    people, allows owners to view their own information and update it. 
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request
        if request.user.is_authenticated():
            return True

        # Write permissions are only allowed to the owner of the snippet
        return obj.owner == request.user
