from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.contrib.gis.db import models

####
#
# Models represent the structure of information stored on the server. 
# The folowing file contains all the models required for this project. 
# The current models are: 
#
#      * Location - Used to store the locations of the users. 
#      * UserStatus - Used to represent information about the users status, 
#        this has several one to one links with other classes. 
#
#      * Friend - 
#      * Song 
#      * Playlist
#      * Friend
#      * 
####

class Location(models.Model):
    """
    A model which holds information about a particular location
    """
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    point = models.PointField()

class UserStatus(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    """
    A model which holds information about the users status
    """
    updated_at = models.DateTimeField(auto_now_add=True)
    test_info = models.CharField(max_length=100, blank=True, default='') #To remove. 
    owner = models.OneToOneField(User, primary_key=True, related_name='user_status', editable=False) 
    geolocation = models.IntegerField(blank=True, null=True) # TODO
    geo_enabled = models.BooleanField(default=False)
    jukebox_mode_enabled = models.BooleanField(default=False) 
    sharing_library_enabled = models.BooleanField(default=False)
    
    # TODO
    #music_library = models.ManyToManyField(Song) 
    #recent_played = models.ForeignKey(Playlist) 
    #jukebox_playlist = models.ForeignKey(Playlist) 

    class Meta:
        ordering = ('updated_at',)

    # If a user is added, this runs. 
    @receiver(post_save, sender=User)
    def create_userstatus_for_user(sender, instance=None, created=False, **kwargs):
        if created:
            UserStatus.objects.get_or_create(owner=instance)

    # Same as above, once a user is deleted the same action is performed for a 
    # userStatus user. 
    @receiver(pre_delete, sender=User)
    def delete_userstatus_for_user(sender, instance=None, **kwargs):
        if instance:
            userStatus = UserStatus.objects.get(owner=instance)
            userStatus.delete()

class Friend(models.Model):
    """
    A model which holds information about the users friend relationships
    """
    owner = models.OneToOneField(User, primary_key=True, related_name='user_friends', editable=False)
    friends = models.ManyToManyField(User, related_name='friend', symmetrical=False)

    # If a user is added, this runs. 
    @receiver(post_save, sender=User)
    def create_friend_for_user(sender, instance=None, created=False, **kwargs):
        if created:
            Friend.objects.get_or_create(owner=instance)

    # Same as above, but for deletion 
    @receiver(pre_delete, sender=User)
    def delete_friend_for_user(sender, instance=None, **kwargs):
        if instance:
            friend = Friend.objects.get(owner=instance)
            friend.delete()

# This is also the information that the mobile side application needs to 
# to represent a song. 
class Song(models.Model):
    """
    A model which holds information about the songs.
    """
    #id = models.AutoField(primary_key=True) #I know this is automatically defined 
    # But declaring ID here means when we are casting json objects later we can 
    # make a reference to it. 
    title = models.CharField(max_length=150, blank=True, default='')
    artist = models.CharField(max_length=150, blank=True, default='')
    album = models.CharField(max_length=150, blank=True, default='')
    albumId = models.CharField(max_length=150, blank=True, default='')
    num_votes = models.IntegerField(default=0, blank=True)
    cleared = models.BooleanField(default=False, blank=True)

    class Meta:
        ordering = ('title',)
        #managed=True

# A list of songs the user is currently playing. 
class Playlist(models.Model):
    
    # TODO want to use this as recent, and use within Jukebox
    title = models.CharField(max_length=150, blank=True, default='')
    #songs = models.ManyToManyField(Song)

class Jukebox(models.Model):
    """
    A model which holds information about the users friend relationships
    """
    owner = models.OneToOneField(User, primary_key=True, related_name='user_jukebox', editable=False)
    title = models.CharField(max_length=150, blank=True, default='')
    enabled = models.BooleanField(default=False)

    # TODO We also want a playlist. 
    # TODO playlist has songs.
         # TODO, posting songs to this, friends can view it. 

    # If a user is added, this runs. 
    @receiver(post_save, sender=User)
    def create_friend_for_user(sender, instance=None, created=False, **kwargs):
        if created:
            Jukebox.objects.get_or_create(owner=instance)

    # Same as above, but for deletion 
    @receiver(pre_delete, sender=User)
    def delete_friend_for_user(sender, instance=None, **kwargs):
        if instance:
            Jukebox.objects.get(owner=instance).delete()

class Messages(models.Model):
    owner = models.OneToOneField(User, primary_key=True, related_name='user_messages', editable=False) 
    #suggested_songs = models.ForeignKey(Song, null=True, blank=True)
    suggested_songs = models.ManyToManyField(Song, related_name='suggested_songs')
    #,null=True, blank=True)

    # If a user is added, this runs. 
    @receiver(post_save, sender=User)
    def create_friend_for_user(sender, instance=None, created=False, **kwargs):
        if created:
            Messages.objects.get_or_create(owner=instance)

    # Same as above, but for deletion 
    @receiver(pre_delete, sender=User)
    def delete_friend_for_user(sender, instance=None, **kwargs):
        if instance:
           Messages.objects.get(owner=instance).delete()

class MusicLibrary(models.Model):
    owner = models.OneToOneField(User, primary_key=True, related_name='user_library', editable=False)
    songs = models.ManyToManyField(User, related_name='library_songs')
    #otherSongs = models.ForeignKey('Song')

    # If a user is added, this runs. 
    @receiver(post_save, sender=User)
    def create_friend_for_user(sender, instance=None, created=False, **kwargs):
        if created:
            MusicLibrary.objects.get_or_create(owner=instance)

    # Same as above, but for deletion 
    @receiver(pre_delete, sender=User)
    def delete_friend_for_user(sender, instance=None, **kwargs):
        if instance:
            MusicLibrary.objects.get(owner=instance).delete()
